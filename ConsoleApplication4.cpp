﻿#include <iostream>
#include <string>

using namespace std;

template<typename T>
class Stack
{
private:
	int NumArray = 0;
	T* StackArray = new T[NumArray];


public:
	Stack()
	{
	
	}
	//Добавляет элемент
	void Push(T Value)
	{
		T* NewArray = new T[NumArray + 1];
		cout << "Добавлен элемент " << Value << endl;

		for (int i = 0; i < NumArray; i++)
		{
			NewArray[i] = StackArray[i];
		}
		NewArray[NumArray] = Value;
		delete[] StackArray;
		NumArray++;
		StackArray = NewArray;
	}
	//Удаляет последний элемент
	void Del()
	{
		cout << "Удалён последний элемент " << endl;
		T* NewArray = new T[NumArray - 1];
		for (int i = 0; i < NumArray - 1; i++)
		{
			NewArray[i] = StackArray[i];;
		}
		delete[] StackArray;
		NumArray--;
		StackArray = NewArray;
	}

	//Достать верхний элемент из стека
	T Pop()
	{
		return StackArray[NumArray - 1];
	}

	//Печатает массив
	void Print()
	{
		for (int i = 0; i < NumArray; i++)
		{

			cout << *(StackArray + i) << "   ";
		}

		cout << endl;
	}
	//Чистим память
	~Stack()
	{
		delete[] StackArray;
	}

};

int main() 
{
	setlocale(LC_ALL, "rus");
	Stack<int> a;
	a.Push(1), a.Push(2), a.Push(3), a.Push(4);
	a.Pop();
	a.Del();
	a.Print();
	//a.~Stack();
}